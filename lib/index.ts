import * as pdfMake from "pdfmake/build/pdfmake";
// import * as pdfFonts from './assets/fonts/vfs_fonts.js';
import * as pdfFonts from "pdfmake/build/vfs_fonts";
import {TDocumentDefinitions} from "pdfmake/interfaces";
import {TCreatedPdf} from "pdfmake/build/pdfmake";


(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

// (pdfMake as any).fonts = {
//     calibri: {
//         normal: 'calibri.ttf',
//         bold: 'calibrib.ttf',
//         italics: 'calibrii.ttf',
//         bolditalics: 'calibriz.ttf',
//     },
// };
  class makePdf{
    public pageSize = 505;
constructor() {
}

 public downloadReceipt(companyName: string = 'company1', logo?: string, checkItems?: [],) {

    const docDefinition: TDocumentDefinitions = {
        pageSize: {
            width: this.pageSize,
            height: 'auto'
        },
        pageMargins: [ 10, 60, 10, 60 ],
        content: [

            {text: `text number 1`, style: 'header'},
            {
                columns: [
                    {text: `text number 2:
                    ${companyName}`, width: this.pageSize / 2 - 30, margin: [0, 0, 10, 0] },
                    {text:
                            `text number 2:
                    text number name
                   text number login name
                    text number email`, width: 'auto'}
                ],
                margin: [0, 20, 0, 20],
            },

            {
                table: {
                    widths: ['*'],
                    body: [[' '], [' ']],
                },
                layout: {
                    hLineWidth(i, node) {
                        return i === 0 || i === node.table.body.length ? 0 : 2;
                    },
                    vLineWidth(i, node) {
                        return 0;
                    },
                },
            },

        ],

        styles: {
            header: {
                fontSize: 26,
                alignment: 'center',
            },
            subheader: {
                alignment: 'center',
                margin: [0, 20, 0, 20],
            },
            bold: {
                fontSize: 16,
                bold: true,
                margin: [0, 0, 0, 20],
            },
            textRight: {
                alignment: 'right',
            },
            marginBottom: {
                margin: [0, 0, 0, 20],
            },
        },
    };

    const pdfObj: TCreatedPdf = pdfMake.createPdf(docDefinition);

    const filename = `document_${new Date()}`;

     console.log("pdfObj d",pdfObj)
     console.log("filename ++++",filename)
     pdfObj.print()
     // pdfMake.createPdf(docDefinition);
     //  pdfMake.createPdf(docDefinition).download(filename);

/*    forkJoin([
        of(this.check),
        this.api.get<Transaction>(`transaction/${this.transactionId}`),
        this.api.get<Settings>(`settings`)
    ])
        .pipe(untilDestroyed(this))
        .subscribe(
            async ([check, transaction, settings]) => {
                const loyalty = settings.loyalty;
                companyName = loyalty.title;
                logo =
                    loyalty && loyalty.photos && loyalty.photos.length
                        ? await this.getBase64ImageFromURL(loyalty.photos[0])
                        : '';

                check.check_items.forEach((item) => {
                    checkItems.push({
                        columns: [
                            {
                                width: '15%',
                                text: `${item.title}`,
                            },
                            [
                                `${item.title}: ${this.getPumpNozzle(item)}`,
                                {
                                    columns: [
                                        {
                                            text: `${item.quantity}${item.unit || ''} * ${this.currencyPipe.transform(
                                                item.calculated_price,
                                                item.price_decimal_digits,
                                                transaction.special_account_currency
                                            )}${item.unit ? ' / ' + item.unit : ''}`,  width: '*',
                                        },
                                        {
                                            text: `${this.currencyPipe.transform(
                                                item.amount,
                                                item.amount_decimal_digits,
                                                transaction.special_account_currency
                                            )}`,
                                            width: 'auto',
                                            style: 'textRight',
                                        },
                                    ],
                                },
                                {
                                    columns: [
                                        {text: `${this.languages.translate.instant('transaction_discount_amount')}`,  width: '*', style: 'marginBottom'},
                                        {
                                            text: `${this.currencyPipe.transform(
                                                item.discount_amount,
                                                item.bonuses_decimal_digits,
                                                transaction.special_account_currency
                                            )}`,
                                            width: 'auto',
                                            style: ['textRight', 'marginBottom'],
                                        },
                                    ],
                                },
                            ],
                        ],
                    });
                });
                let pointOfSale;
                if (check.pos) {
                    let phonesStr = '';
                    if (check.pos.phones && check.pos.phones.length) {
                        phonesStr = ', ';
                        check.pos.phones.forEach((value, index) => {
                            phonesStr += (value.number ? value.number : value) + (index < check.pos.phones.length - 1 ? ', ' : '');
                        });
                    }
                    pointOfSale = {
                        text:
                            `${this.languages.translate.instant('transaction_pos_title')}: ` +
                            `${check.pos.title}, ${check.pos.addresses ? check.pos.addresses : ''}${phonesStr}`,
                        margin: [0, 0, 0, 20],
                    };
                } else {
                    pointOfSale = '';
                }

                const docDefinition: TDocumentDefinitions = {
                    pageSize: {
                        width: this.pageSize,
                        height: this.pageSize === PRINT_SIZE.PRINTA4 ? this.printHeight : 'auto'
                    },
                    pageMargins: [ 10, 60, 10, 60 ],
                    content: [
                        logo && {image: logo, width: 50, height: 50, alignment: 'center', margin: [0, 0, 0, 20]},
                        {text: `${this.languages.translate.instant('receipt')}`, style: 'header'},
                        {
                            columns: [
                                {text: `${this.languages.translate.instant('from')}:
                    ${companyName}`, width: this.pageSize / 2 - 30, margin: [0, 0, 10, 0] },
                                {text:
                                        `${this.languages.translate.instant('receipt_to')}:
                    ${transaction.user_name}
                    ${transaction.user_phone}
                    ${transaction.user_email}`, width: 'auto'}
                            ],
                            margin: [0, 20, 0, 20],
                        },
                        {text: `${this.languages.translate.instant('TransactionID')}: ${transaction.id}`},
                        {text: `${this.languages.translate.instant('Date')}: ${this.datePipe.transform(transaction.processed, 'short')}`},
                        {text: `${this.languages.translate.instant('receipt_print_date')}: ${this.datePipe.transform(new Date(), 'short')}`},
                        pointOfSale,
                        ...checkItems,
                        {
                            columns: [
                                {text: '', width: '15%'},
                                [
                                    {
                                        columns: [
                                            {text: `${this.languages.translate.instant('receipt_total')}`, width: '*'},
                                            {
                                                text: `${this.currencyPipe.transform(
                                                    check.amount,
                                                    transaction.amount_decimal_digits,
                                                    transaction.special_account_currency
                                                )}`,
                                                width: 'auto',
                                                style: 'textRight',
                                            },
                                        ],
                                    },
                                    {
                                        columns: [
                                            {text: `${this.languages.translate.instant('transaction_discount_amount')}`, width: '*'},
                                            {
                                                text: `${this.currencyPipe.transform(
                                                    transaction.discount_amount,
                                                    transaction.bonuses_decimal_digits,
                                                    transaction.special_account_currency
                                                )}`,
                                                width: 'auto',
                                                style: 'textRight',
                                            },
                                        ],
                                    },
                                    {
                                        columns: [
                                            {text: `${this.languages.translate.instant('transaction_discount_amount')}`, width: '*'},
                                            {
                                                text: `${this.currencyPipe.transform(
                                                    check.bonuses_redeemed_amount,
                                                    check.bonuses_decimal_digits,
                                                    transaction.special_account_currency
                                                )}`,
                                                width: 'auto',
                                                style: 'textRight',
                                            },
                                        ],
                                    },
                                ],
                            ],
                        },
                        {
                            table: {
                                widths: ['*'],
                                body: [[' '], [' ']],
                            },
                            layout: {
                                hLineWidth(i, node) {
                                    return i === 0 || i === node.table.body.length ? 0 : 2;
                                },
                                vLineWidth(i, node) {
                                    return 0;
                                },
                            },
                        },
                        // amount to pay
                        {
                            columns: [
                                {text: '', width: '15%'},
                                [
                                    {
                                        columns: [
                                            {text: `${this.languages.translate.instant('receipt_total')}`,  width: '*', style: 'bold'},
                                            {
                                                text: `${this.currencyPipe.transform(
                                                    check.amount_to_pay,
                                                    check.amount_decimal_digits,
                                                    transaction.special_account_currency
                                                )}`,
                                                width: 'auto',
                                                style: ['bold', 'textRight'],
                                            },
                                        ],
                                    },
                                ],
                            ],
                        },
                        // paid
                        {
                            columns: [
                                {
                                    text: `${this.languages.translate.instant('receipt_paid')}`,
                                    width: '15%',
                                },
                                [
                                    {
                                        columns: [
                                            {
                                                text: `${this.languages.translate.instant('receipt_from_account')} ${companyName}, ${
                                                    transaction.special_account_title
                                                }`, width: '*'
                                            },
                                            {
                                                text: `${this.currencyPipe.transform(
                                                    transaction.amount_to_pay,
                                                    transaction.amount_decimal_digits,
                                                    transaction.special_account_currency
                                                )}`,
                                                width: 'auto',
                                                style: 'textRight',
                                            },
                                        ],
                                    },
                                    {
                                        columns: [
                                            {text: `${this.languages.translate.instant('receipt_bonuses_earned')}`, width: '*'},
                                            {
                                                text: `${this.currencyPipe.transform(
                                                    check.bonuses_added_amount,
                                                    check.bonuses_decimal_digits,
                                                    transaction.special_account_currency
                                                )}`,
                                                width: 'auto',
                                                style: 'textRight',
                                            },
                                        ],
                                    },
                                    {
                                        columns: [
                                            {text: `${this.languages.translate.instant('receipt_bonus_account')}`, width: '*'},
                                            {
                                                text: `${this.currencyPipe.transform(
                                                    transaction.bonuses_on_account,
                                                    transaction.bonuses_decimal_digits,
                                                    transaction.special_account_currency
                                                )}`,
                                                width: 'auto',
                                                style: 'textRight',
                                            },
                                        ],
                                    },
                                ],
                            ],
                        },
                        ...(check.odometer /!*|| check.corporate_vehicle*!/
                            ? [
                                {
                                    table: {
                                        widths: ['*'],
                                        body: [[' '], [' ']],
                                    },
                                    layout: {
                                        hLineWidth(i, node) {
                                            return i === 0 || i === node.table.body.length ? 0 : 2;
                                        },
                                        vLineWidth(i, node) {
                                            return 0;
                                        },
                                    },
                                },
                            ]
                            : []),
                        // vehicle
                        /!*{
                          columns: [
                            {
                              width: '15%',
                              text: '',
                            },
                            [
                              ...(check.odometer
                                ? [
                                  {
                                    columns: [
                                      { text: `${this.languages.translate.instant('transaction_odometer')}` },
                                      { text: `${check.odometer || ''}`, width: '15%', style: 'textRight' },
                                    ],
                                  },
                                ]
                                : []),

                              ...((check.corporate_vehicle && check.corporate_vehicle.qr_code) || check.vehicle_id
                                ? [
                                  {
                                    columns: [
                                      { text: `${this.languages.translate.instant('ReceiptPage#Vehicle_inventory_code')}` },
                                      {
                                        text: `${check.corporate_vehicle ? check.corporate_vehicle.qr_code : check.vehicle_id || ''}`,
                                        width: '15%',
                                        style: 'textRight',
                                      },
                                    ],
                                  },
                                ]
                                : []),
                              ...(check.corporate_vehicle && check.corporate_vehicle.description
                                ? [
                                  {
                                    columns: [
                                      { text: `${this.languages.translate.instant('receipt_vehicle_description')}` },
                                      { text: `${check.corporate_vehicle.description}`, width: '15%', style: 'textRight' },
                                    ],
                                  },
                                ]
                                : []),
                              ...(check.corporate_vehicle && check.corporate_vehicle.vehicle_id
                                ? [
                                  {
                                    columns: [
                                      { text: `${this.languages.translate.instant('receipt_vehicle_id')}` },
                                      { text: `${check.corporate_vehicle.vehicle_id}`, width: '15%', style: 'textRight' },
                                    ],
                                  },
                                ]
                                : []),
                              // ...(check.corporate_vehicle && check.corporate_vehicle.position ? [
                              //   {
                              //     columns: [
                              //       { text: `${this.languages.translate.instant('Employee position')}` },
                              //       { text: `${check.corporate_vehicle.position.title}`, width: '15%', style: 'textRight' },
                              //     ]
                              //   }
                              // ] : [])
                            ],
                          ],
                        },*!/
                    ],
                    defaultStyle: {
                        font: 'calibri',
                    },
                    styles: {
                        header: {
                            fontSize: 26,
                            alignment: 'center',
                        },
                        subheader: {
                            alignment: 'center',
                            margin: [0, 20, 0, 20],
                        },
                        bold: {
                            fontSize: 16,
                            bold: true,
                            margin: [0, 0, 0, 20],
                        },
                        textRight: {
                            alignment: 'right',
                        },
                        marginBottom: {
                            margin: [0, 0, 0, 20],
                        },
                    },
                };

                const pdfObj: TCreatedPdf = pdfMake.createPdf(docDefinition);

                const filename = `${this.languages.translate.instant('receipt')}_${transaction.id}`;
                this.downloadService.download(pdfObj, filename);
            },
            () => {
                this.loading.hide();
            }
        );*/
}
}
const g = new makePdf()
 export   default g

// var o = require('./dist')

